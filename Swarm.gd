extends Node

func _process(delta):
	if get_children().size() == 1:
		get_node("../Text").fly_text("Level Cleared")
		new_level()
	

func new_level():
	var enemy_types = [	{	monster = "skully",
							points = 50,
						},
						{	monster = "cthulhy",
							points = 20,
						},
						{	monster = "medussy",
							points = 10,
						},]	
	
	var row_y_location = 0
	
	for alien in enemy_types:
		for _j in range (2):
			for i in range(10):
				var enemy = preload("res://Enemy.tscn").instance()
				add_child(enemy)
				enemy.start(Vector2((i * 64) + 50, row_y_location + 50), alien)
			row_y_location += 64
	
	get_node("../Text").fly_text("Get Ready!")

func _on_Limits_area_entered(_area):
	if $CollisionTimer.is_stopped():
		$CollisionTimer.start()
		get_tree().call_group("enemies", "switch_direction")

func _on_Turret_area_entered(_area):
	get_tree().call_group("enemies", "stop")

func _on_Turret_new_life():
	get_tree().call_group("enemies", "restart")
