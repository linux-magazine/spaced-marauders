extends Area2D

signal fire

var rng = RandomNumberGenerator.new()

var speed = 80
var moving = true
var direction = 1
var animation #= "medussy" # "medussy" for testing purposes only
var fire_chance
var points

func _ready():
	rng.randomize()
	#position = Vector2(50, 50) # For testing purposes only

func start(start_position, alien):
	position = start_position
	animation = alien.monster
	$AnimatedSprite.play(animation)
	
	# The more points the alien is valued at, the more aggressive it is. It fires
	# more frequently and the wait time between barrages of shots is shorter	
	points = alien.points
	$FireDelay.wait_time = rng.randf_range(500 / points, (500 / points) + 10) 
	fire_chance = alien.points / 100.0

func _process(delta):
	if moving:
		position.x += direction * (speed * delta)
	$AnimatedSprite.play(animation)
	
	# This here avoids aliens of starting the level with a barrage of shots.
	# The timer is stopped and won't start until a randomly generated number is
	# lower than the chance the alien has to fire.
	if moving:
		if rng.randf() < fire_chance and $FireDelay.is_stopped():
			$FireDelay.start()

func switch_direction():
	direction = -direction
	position.y += 10

func stop():
	moving = false

func destroy():
	if moving:
		get_tree().set_group("enemies", "speed", speed + 5)
		get_tree().set_group("enemies", "fire_chance", fire_chance + 0.05)
	get_parent().get_parent().score += points
	hide()
	queue_free()

func _on_FireDelay_timeout():
	if rng.randf() < fire_chance and moving:
		var ray = preload("res://Ray.tscn").instance()
		get_parent().add_child(ray)
		ray.start(position)

func restart():
	moving = true
	get_tree().set_group("enemies", "speed", speed)
