extends Area2D

var speed = 400

func start(start_pos):
	position.x = start_pos.x
	position.y = start_pos.y + 10
	$Phew.play()

func _process(delta):
	position.y += delta * speed

func _on_VisibilityNotifier2D_screen_exited():
	queue_free()
