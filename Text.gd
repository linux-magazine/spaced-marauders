extends CanvasLayer

func _ready():
	pass

func update_lives(lives):
	$Lives.text = "Lives: " + str(lives)

func update_score(score):
	$Score.text = str(score)

func fly_text(text):
	$AnimateText/Text.hide()
	$AnimateText/Text.text = text
	$AnimateText/Text.rect_pivot_offset.x = $AnimateText/Text.rect_size.x/2
	$AnimateText/Text.rect_pivot_offset.y = $AnimateText/Text.rect_size.y/2
	$AnimateText/Text.show()
	
	get_tree().paused = true
	for i in range(0, len(text)):
		$AnimateText/Text.visible_characters=i
		yield(get_tree().create_timer(0.1), "timeout")
	$AnimateText.play("Fade")
	yield(get_tree().create_timer(3), "timeout")
	get_tree().paused = false
