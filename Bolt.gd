extends Area2D

var speed = 400

func _process(delta):
	position.y -= speed * delta
	
func start (bolt_position):
	position = bolt_position

func _on_VisibilityNotifier2D_screen_exited():
	queue_free()

func _on_Bolt_area_shape_entered(_area_id, area, _area_shape, _self_shape):
	area.destroy()
	speed = 0
	$CollisionShape2D.set_deferred("disabled", true)
	$AnimatedSprite.play("explosion")
	$AudioStreamPlayer2D.play()
	yield($AnimatedSprite, "animation_finished")
	hide()
	queue_free()
