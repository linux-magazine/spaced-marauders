extends Node

var score = 0
var lives = 3

func _process(delta):
	$Text.update_score(score)	

func _ready():
	$Swarm.new_level()
	$Text.update_score(score)
	$Text.update_lives(lives)

func _on_Turret_fire():
	var bolt = preload("res://Bolt.tscn").instance()
	add_child(bolt)
	bolt.start(Vector2($Turret.position.x, $Turret.position.y - 20))
	
