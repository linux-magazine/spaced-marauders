extends Area2D

func _ready():
	var screen_size = get_viewport_rect().size
	$Left.shape.a = Vector2 (0, 0)
	$Left.shape.b = Vector2 (0, screen_size.y)
	
	$Right.shape.a = Vector2 (screen_size.x, 0)
	$Right.shape.b = Vector2 (screen_size.x,screen_size.y)
