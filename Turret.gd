extends Area2D

signal fire
signal new_life

var speed
var screen_size
var sides = Vector2()

func _ready():
	speed = 400
	screen_size = get_viewport_rect().size
	sides = Vector2(32, screen_size.x - 32)
	position = (Vector2(screen_size.x/2, screen_size.y-32))
	
	$AnimatedSprite.play("fire", true)
	show()
	get_node("../Text").fly_text("Get Ready!")
	set_deferred("disabled", false)

	
func _process(delta):
	var velocity = Vector2(0, 0)
	
	if Input.is_action_pressed("ui_right"):
		velocity.x += 1
	if Input.is_action_pressed("ui_left"):
		velocity.x -= 1
	if velocity.length() > 0:
		velocity = velocity.normalized() * speed
	if Input.is_action_pressed("ui_fire"):
		if $FireDelay.is_stopped():
			$FireDelay.start()
			$AnimatedSprite.play("fire", true)
			$Pew.play()
			emit_signal("fire")
	
	position += velocity * delta
	position.x = clamp(position.x, sides.x, sides.y)

func _on_Turret_area_entered(_area):
	destroy()
	
func _on_BottomLimit_area_entered(_area):
	destroy()

func destroy():
	set_deferred("disabled", true)
	get_tree().call_group("enemies", "stop") 
	speed = 0

	$AnimatedSprite.play("explosion")
	$KaBoom.play()
	yield($AnimatedSprite, "animation_finished")
	
	hide()
	get_parent().lives -= 1
	get_node("../Text").update_lives(get_parent().lives)
	if get_parent().lives == 0:
		get_node("../Text").fly_text("Game Over")
		queue_free()
	else:
		emit_signal("new_life")
		_ready()
