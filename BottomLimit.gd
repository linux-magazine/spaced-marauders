extends Area2D

func _ready():
	var screen_size = get_viewport_rect().size
	$Bottom.shape.a = Vector2 (0, screen_size.y)
	$Bottom.shape.b = Vector2 (screen_size.x, screen_size.y)
